package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {

    // Properties
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    // establish relationship of the property to the Post model
    @ManyToOne
    // sets the relationship to this property and user_id column in the database to the primary key of the User model
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    // Constructors
    // required when retrieving data from the database
    public Post() {}

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
